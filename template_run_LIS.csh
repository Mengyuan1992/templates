#!/bin/bash

# Get the forcing data
cat > ./getforc.pbs << EOF_getforc
#!/bin/bash
#PBS -m ae
#PBS -M ${EMAIL}
#PBS -P ${PROJECT}
#PBS -q express
#PBS -l walltime=00:30:00
#PBS -l mem=100MB
#PBS -l ncpus=1
#PBS -j oe
#PBS -l wd
#PBS -l storage=gdata/${PROJECT}+scratch/${PROJECT}

cd ${LIS_PATH}

# get input data if first month
# else
# get the restart file and monthly input data if not first month
if [ ${FIRST} -eq 1 ]
then
    # Get python code for post-processing
    ln -sf ${DECK_PATH}/post-process.py .
    es=\$?
    [ \$es -ne 0 ] && exit \$es

    # Get MODEL_OUTPUT_LIST.TBL to run directory
    cp -f ${DECK_PATH}/${MODEL_OUTPUT_AR} ${LIS_PATH}/.
    es=\$?
    [ \$es -ne 0 ] && exit \$es
else
    # Get restart file(s)
    dom=1
    while [ \$dom -le ${max_dom} ]
    do
  	ln -sf ${LISOUT_PATH}/LIS_RST_${LSM}_${SYEAR}${SMONTH}${SDAY}0000.d0\${dom}.nc .
	es=\$?
	[ \$es -ne 0 ] && exit \$es
	dom=\$[\$dom+1]
    done
fi

# get lis.config file for this month
ln -sf ${DECK_PATH}/${LIS_CONFIG} .


# get next script
ln -sf ${DECK_PATH}/run_LIS_${NYEAR}_${NMONTH} .

exit
EOF_getforc


# Run LIS
cat > ./runLIS.pbs <<EOF_run_LIS
#!/bin/bash
#PBS -m ae
#PBS -M ${EMAIL}
#PBS -P ${PROJECT}
#PBS -l walltime=12:00:00
#PBS -l mem=200GB
#PBS -l ncpus=${NPROCS}
#PBS -j oe
#PBS -q normal
#PBS -l wd
#PBS -l storage=gdata/hh5+gdata/${PROJECT}+scratch/${PROJECT}+gdata/w35+gdata/ub4

codepath=${CODEDIR_ROOT}/installs/${EXE_DIR}/WRFV3/lis/make
if [ -x \${codepath}"/LIS" ]; then
    export PATH="\${codepath}:\${PATH}"
else
    echo "ERROR: LIS not found"
    exit 1
fi

module purge
module load openmpi/4.0.2
module load netcdf/4.7.1

# Go to the run directory
cd ${LIS_PATH}/.

# Run LIS
mpirun -np \$PBS_NCPUS LIS -f ${LIS_CONFIG}

exit
EOF_run_LIS

# Put the results at CCRC
cat > ./putout.pbs << EOF_putout
#!/bin/csh
#PBS -m ae
#PBS -M ${EMAIL}
#PBS -P ${PROJECT}
#PBS -q normalbw
#PBS -l walltime=24:00:00
#PBS -l mem=20GB
#PBS -l ncpus=8
#PBS -j oe
#PBS -l wd
#PBS -l storage=gdata/hh5+gdata/${PROJECT}+scratch/${PROJECT}
#PBS -l jobfs=100GB

module load nco/4.7.7
module use /g/data3/hh5/public/modules
module load conda/analysis3

cd ${LIS_PATH}/
cd ${OUTPUT_DIR}
set dom=1
while ( \$dom <= ${max_dom} )
    mv SURFACEMODEL.d0\${dom}.stats ${LSM}.d0\${dom}.stats.${SYEAR}${SMONTH}
    @ dom++
end

# Call python code
python ${LIS_PATH}/post-process.py --orig_levels -o ${LISOUT_PATH}/. -s ${SYEAR}-${SMONTH} -e ${NYEAR}-${NMONTH} ${LIS_PATH}/${OUTPUT_DIR}/SURFACEMODEL
set es=\$?
[ \$es -ne 0 ] && exit \$es
 

# Move all restart files to this level.
mv SURFACEMODEL/LIS_RST* .

# Transfer


#get files with rsync - if any of them fail then fail out of the pbs job
rm -r SURFACEMODEL

cd ../
cp ${OUTPUT_DIR}/* ${LISOUT_PATH}/.
set es=\$?
[ \$es -ne 0 ] && exit \$es

# Remove outputs from rundirectory
rm ${OUTPUT_DIR}/*

# Remove last restart file if any
rm ${LIS_PATH}/LIS_RST*


# Call next script.
./run_LIS_${NYEAR}_${NMONTH} >& log_${NYEAR}_${NMONTH}

exit
EOF_putout

getjobid=`qsub getforc.pbs`
runjobid=`qsub -W depend=afterok:$getjobid runLIS.pbs`
qsub -W depend=afterok:$runjobid putout.pbs


exit
