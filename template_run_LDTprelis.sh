#!/bin/bash

# Run LDT
#PBS -m ae
#PBS -M ${EMAIL}
#PBS -P ${PROJECT}
#PBS -l walltime=2:20:00
#PBS -l mem=8GB
#PBS -l ncpus=1
#PBS -j oe
#PBS -q express
#PBS -l wd
#PBS -l storage=gdata/hh5+gdata/${PROJECT}+scratch/${PROJECT}+gdata/w35+gdata/ub4

codepath=${CODEDIR_ROOT}/installs/${EXE_DIR}/ldt/make
if [ -x ${codepath}"/LDT" ]; then
    export PATH="${codepath}:${PATH}"
else
    echo "ERROR: LDT not found"
    exit 1
fi

module purge
module load pbs
module load openmpi/4.0.2
module load netcdf/4.7.1

ln -s "/g/data/w35/ccc561/LIS-WRF/CABLE_AUX-dev/offline" GSWP3_data
ln -s "/g/data/w35/ccc561/LIS-WRF/CABLE_soilparams/Harmonized_Global_Soil_Data/data" HWSD_data

# Run LIS
LDT ldt.config.prelis

cp ${LIS_DIR}/lis_input* ${BDYDATA_PATH}

